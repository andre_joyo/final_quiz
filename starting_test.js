/*last_html=questions[0]["radio"][0]["HTML"].length;
last_css=questions[0]["radio"][1]["CSS"].length;
last_javascript=questions[0]["radio"][2]["Javascript"].length;*/
var index_questions=[];
var index_question=[];
function random_int(a, b) {
    var num=Math.random();
    var range=b-a;
    var random=a+range*num;
    random=Math.round(random);
    return random;
}
function obtain_indexes(last_element) {
    var actual=0;
    var duplicated;
    var language;
    var index;
    while (actual<7){
        duplicated=false;
        index_question[actual] = random_int(0, last_element);
        index=0;
        while(index<actual && !duplicated) {
            if (index_question[actual]==index_question[index]){
                duplicated=true;
            }    
            index++;    
        }
        if (!duplicated){
            actual++;
        }
    }    
}
var key="";
function get_test() {
    var last_element;
    var node; 
    var node_q; 
    var node_content;
    var i_quest;
    var question_content=["enunciate", "a", "b", "c", "d", "answer"]
    var node_div_option=[];
    var node_option=[];
    var node_text=[];
    var test="";    
    technology=["HTML", "CSS", "Javascript"];
    for (let i = 0; i < technology.length; i++) {
        last_element=questions[0]["radio"][i][technology[i]].length-1 ;
        console.log("Last question index"+technology[i]+": "+last_element)
        obtain_indexes(last_element);    
        for (let j = 0; j < 7; j++) {
            //console.log(index_question[j])
            i_quest=i*7+j;
            index_questions[i_quest]=index_question[j];
            //console.log(questions[0]["radio"][i][technology[i]][index_question[j]])
            //Añadir divs contenedores de preguntas
            node = document.createElement("DIV");
            node.className ="the_question";
            node.id ="the_question"+i_quest;
            document.getElementById("questions").appendChild(node);
            //Añadir enunciado
            node_p = document.createElement("P"); 
            node_p.innerText=JSON.stringify(questions[0]["radio"][i][technology[i]][index_question[j]]["enunciate"]);
            document.getElementById("the_question"+i_quest).appendChild(node_p);
            //Añadir div de opciones
            node_options = document.createElement("DIV");
            node_options.className ="the_question_options";
            node_options.id ="the_question"+i_quest+"_options";
            document.getElementById("the_question"+i_quest).appendChild(node_options);
            //console.log(Object.keys(questions[0]["radio"][i][technology[i]][index_question[j]]).length);
            for (let k = 1; k<Object.keys(questions[0]["radio"][i][technology[i]][index_question[j]]).length-1; k++) {
                //Añadir div de opción                
                node_div_option[k]=document.createElement("DIV");
                node_div_option[k].id = "the_question"+i_quest+"_option"+k;
                document.getElementById("the_question"+i_quest+"_options").appendChild(node_div_option[k]);
                //Crear radio button y añadirlo al div de opción
                node_option[k] = document.createElement("INPUT");
                node_option[k].setAttribute('type',"radio");
                node_option[k].setAttribute('value',question_content[k]); //ccccccccccccccambiar valor
                node_option[k].id="the_question"+i_quest+"_option"+k;
                node_option[k].name="question_"+i_quest;
                document.getElementById("the_question"+i_quest+"_option"+k).appendChild(node_option[k]);
                //Crear y añadir span con texto asociado para el radio button
                node_text[k]=document.createElement("span");
                node_text[k].innerText=questions[0]["radio"][i][technology[i]][index_question[j]][question_content[k]];
                document.getElementById("the_question"+i_quest+"_option"+k).appendChild(node_text[k]);
            }
            test=test+JSON.stringify(questions[0]["radio"][i][technology[i]][index_question[j]]);
        }   
    }

    //Asignar cuestionario a usuario en localStorage
    var go_index=true;
    for ( var i = 0; i < localStorage.length; i++ ) {//console.log(localStorage.key(i));//console.log(localStorage.getItem(localStorage.key(i)));
        if( localStorage.getItem( localStorage.key( i )) =="test" ){
            key=localStorage.key( i );
            localStorage.setItem(key, test);
            go_index=false;
        } 
    }
    if (go_index==true){
        window.location.href = 'index.html';
    }
}
function create_questions_bar() {
    for (let i = 0; i < index_questions.length; i++) {
        //Crear barra de preguntas
        node_q = document.createElement("DIV");
        node_q.className ="question";
        node_q.id ="question"+i;
        if (i==7 || i==14){
            node_q.className ="question m-l";
        }
        document.getElementById("questions_bar").appendChild(node_q);
    }    
    bar_functionality();
}
function print_bar(answered, id_question) {
    //Marcar preguntas contestadas, vistas, no vistas.
    if(answered){
        document.getElementById(id_question).style.backgroundColor = "#4EBA53";                    
    }
    else{
        document.getElementById(id_question).style.backgroundColor = "#FFBB33";
    }
}
var correct_answers=[];
for (let i = 0; i < questions.length; i++) {
    correct_answers[i]=false;
}
var id_question="question0";
var answer;
var question=[];
function bar_functionality() {
    var question=document.getElementsByClassName("question");
    var answered;
    document.getElementById(id_question).style.backgroundColor = "chartreuse";
    for (let i = 0; i < question.length; i++) {
        question[i].addEventListener("click", function(){//console.log("bar clicked");
            //Animación al cambiar de pregunta
            body=document.getElementsByTagName("body")[0];
            body.className ="animated";
            setTimeout(function(){
                body.classList.remove("animated");
            }, 1000);    
            number_question=parseInt(id_question.slice(8));
            //Comprobar si está respondida(radio)
            radio_b = document.getElementsByName("question_"+number_question);
            answered = false;
            for(k=0;k<radio_b.length;k++){
                if(radio_b[k].checked){
                    answered=true;
                }
            }
            print_bar(answered, id_question);
            //pregunta anterior
            document.getElementById("the_"+id_question).style.display = "none"; //ocultarla

            //pregunta actual
            event.target.style.backgroundColor =  "chartreuse";                 //resaltar pregunta en barra
            id_question = event.target.id;
            document.getElementById("the_"+id_question).style.display = "block";//mostrarla
            number_question=parseInt(id_question.slice(8))+1;
            //Mostrar lenguaje en la cabecera
            document.getElementById("number").innerHTML = number_question;
            if (number_question<=7){
                current_language = "HTML";
                i_current_language ="0";
            } else if(number_question<=14){
                current_language = "CSS";
                i_current_language ="1";
            } else{
                current_language = "Javascript";
                i_current_language ="2";
            }
            document.getElementById("subject").innerHTML = current_language;
        });        
    }
}
var radio_b=[];
function comprove_answers() {
    for (let i = 0; i < index_questions.length; i++) {   //console.log("question legth: "+index_questions.length);
        radio_b=document.getElementsByName("question_"+i);//console.log(radio_b);
        var ans_selected;
        correct_answers[i]=false;
        for(k=0;k<radio_b.length;k++){
            if(radio_b[k].checked){//console.log(radio_b[k].value);console.log(event.target.id);
                ans_selected=radio_b[k].value;
                ans_selected='"'+ans_selected+'"';
                if (i<7){
                    if (JSON.stringify(questions[0]["radio"][0]["HTML"][index_questions[i]]["answer"])==ans_selected){
                        correct_answers[i]=true;//console.log(JSON.stringify(questions[0]["radio"][0]["HTML"][index_questions[i]]));
                    }
                } else if(i<14){
                    if (JSON.stringify(questions[0]["radio"][1]["CSS"][index_questions[i]]["answer"])==ans_selected){
                        correct_answers[i]=true;//console.log(JSON.stringify(questions[0]["radio"][1]["CSS"][index_questions[i]]));
                    }
                } else{
                    if (JSON.stringify(questions[0]["radio"][2]["Javascript"][index_questions[i]]["answer"])==ans_selected){
                        correct_answers[i]=true;//console.log(JSON.stringify(questions[0]["radio"][2]["Javascript"][index_questions[i]]));
                    }
                }
            }                 //console.log(radio_b[k]);
        }
    }
    finish();
}
function finish() {
    var correct_answers_q=0;
    for (let i = 0; i < correct_answers.length; i++) {
        if (correct_answers[i]==true){
            correct_answers_q=correct_answers_q+1;
        }
    }
    question=document.getElementsByClassName("question")[id_question].click();
    localStorage.setItem(key+"score", correct_answers_q);
    console.log("Puntuación: "+correct_answers_q);
    window.location.href = 'ranking.html';    
}
function start() {
    get_test();
    create_questions_bar();
    document.getElementById("the_question0").style.display="block";//console.log(index_questions);
}