 var questions;
questions=[
            {
                "radio":[
                    {
                        "HTML" : [
                            {
                                "enunciate": "En HTML, la etiqueta utilizada para agrupar campos en un formulario es",
                                "a": "a) <fieldset>",
                                "b": "b) <legend>",
                                "c": "c) <label>",
                                "d": "d) Las anteriores respuestas no son correctas",
                                "answer": "a"
                            }
                            ,{
                                "enunciate": "¿Qué afirmación no es correcta respecto al lenguaje XML?",
                                "a": "a) Los documentos HTML son directamente compatibles con las reglas de XML",
                                "b": "b) XML permite definir nuevas etiquetas y atributos",
                                "c": "c) Los documentos XML pueden ser validados para comprobar si son correctos",
                                "d": "d) En XML la estructura del documento puede anidarse en varios niveles de complejidad",
                                "answer": "a"
                            }                  
                            ,{
                                "enunciate": "En HTML, la forma correcta de crear un comentario es",
                                "a": "a) <-- El comentario -->",
                                "b": "b) <--! El comentario !-->",
                                "c": "c) <--! El comentario -->",
                                "d": "d) <!-- El comentario -->",
                                "answer": "b"
                            }
                            ,{
                                "enunciate": "¿Cuál es la declaración correcta que define la versión de un documento XML?",
                                "a": "a) <?xml version='1.0'?>",
                                "b": "b) <?xml version='1.0' />",
                                "c": "c) <xml version='1.0' />",
                                "d": "d) Las anteriores respuestas no son correctas",
                                "answer": "a"
                            }
                            ,{
                                "enunciate": "¿Cuál de las siguientes afirmaciones sobre XML es correcta?",
                                "a": "a) XML es una extensión multidocumento de HTML",
                                "b": "b) XML es un metalenguaje que se emplea para definir otros lenguajes",
                                "c": "c) XML es la versión ligera de XHTML",
                                "d": "d) Las anteriores respuestas no son correctas",
                                "answer": "b"
                            }
                            ,{
                                "enunciate": "Cuando se escribe una página XHTML",
                                "a": "a) Los valores de los atributos no necesitan estar encerrados entre comillas",
                                "b": "b) Los valores de los atributos necesitan estar encerrados entre comillas",
                                "c": "c) Todas las etiquetas necesitan al menos un atributo",
                                "d": "d) Las anteriores respuestas no son correctas",
                                "answer": "b"
                            }
                            ,{
                                "enunciate": "En HTML, el atributo alt se emplea para",
                                "a": "a) Indicar la URL de una página web donde se proporciona una descripción larga de una imagen",
                                "b": "b) Proporcionar el texto alternativo de una imagen",
                                "c": "c) Indicar la URL de una imagen en un formato alternativo",
                                "d": "d) Las anteriores respuestas no son correctas",
                                "answer": "b"
                            }
                            ,{
                                "enunciate": "En un formulario HTML, cuando se pulsa sobre un botón de tipo submit, los datos introducidos en el formulario se envían a la URL indicada en el atributo del formulario llamado",
                                "a": "a) method",
                                "b": "b) post",
                                "c": "c) target",
                                "d": "d) Las anteriores respuestas no son correctas",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "Respecto el juego de caracteres ISO-8859-1",
                                "a": "a) ISO-8859-1 también se conoce como Latin1",
                                "b": "b) ISO-8859-15 es similar a ISO-8859-1, pero sustituye algunos símbolos poco comunes para incluir, por ejemplo, el símbolo del euro",
                                "c": "c) Incluye todos los símbolos para las principales lenguas de Europa occidental (alemán, castellano, catalán, danés, finés, francés, inglés, noruego, sueco, etc.)",
                                "d": "d) Todas las respuestas son correctas",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "En HTML, el atributo title se emplea para",
                                "a": "a) Definir el título de la página",
                                "b": "b) Definir el nombre de un elemento",
                                "c": "c) Proporcionar información adicional sobre un elemento",
                                "d": "d) Las anteriores respuestas no son correctas",
                                "answer": "a"
                            }
                            ,{
                                "enunciate": "Para provocar la apertura de un enlace en una ventana nueva, se emplea el atributo",
                                "a": " a) target='_parent'",
                                "b": " b) target='_self'",
                                "c": " c) target='_top'",
                                "d": " d) target='_blank'",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "Las etiquetas <ol> y <li> se emplean para crear",
                                "a": " a) Una tabla de datos",
                                "b": " b) Una lista ordenada",
                                "c": " c) Una lista no ordenada",
                                "d": " d) Las anteriores respuestas no son correctas",
                                "answer": "b"
                            }
                            ,{
                                "enunciate": "En HTML, para agrupar elementos en una lista desplegable se emplea",
                                "a": " a) <ul>",
                                "b": " b) <ol>",
                                "c": " c) <fieldset>",
                                "d": " d) <optgroup>",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "En HTML, para definir una línea horizontal de separación se emplea",
                                "a": " a) <dd>",
                                "b": " b) <hr>",
                                "c": " c) <pre>",
                                "d": " d) <xmp>",
                                "answer": "b"
                            }
                            ,{
                                "enunciate": "En HTML, los elementos de una lista desplegable se definen con",
                                "a": " a) <ol>",
                                "b": " b) <ul>",
                                "c": " c) <li>",
                                "d": " d) <option>",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "En HTML, para definir que el idioma principal de una página es el español se emplea",
                                "a": " a) <html lang='es'>",
                                "b": " b) <html id='es'>",
                                "c": " c) <head lang='es'>",
                                "d": " d) <head id='es'>",
                                "answer": "a"
                            }
                            ,{
                                "enunciate": "En una página XHTML, ¿cuál es la forma correcta de hacer referencia a un fichero externo con código JavaScript llamado 'xxx.js'?",
                                "a": " a) <script name='xxx.js' type='text/javascript' />",
                                "b": " b) <script href='xxx.js' type='text/javascript' />",
                                "c": " c) <script src='xxx.js' type='text/javascript' />",
                                "d": " d) Las anteriores respuestas no son correctas",
                                "answer": "c"
                            }
                        ]
                    },
                    {
                        "CSS":[
                            {
                                "enunciate": "¿Cómo se hace en CSS para que el texto aparezca en negrita?",
                                "a": "a) font:b",
                                "b": "b) style:bold",
                                "c": "c) text:bold",
                                "d": "d) font-weight:bold",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "¿Qué propiedad de CSS se emplea para cambiar el tipo de letra de un elemento?",
                                "a": "a) text-type",
                                "b": "b) font-type",
                                "c": "c) text-family",
                                "d": "d) font-family",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "¿Qué significa CSS?",
                                "a": "a) Cascading Style Sheets",
                                "b": "b) Creative Style Sheets",
                                "c": "c) Computer Style Sheets",
                                "d": "d) Colorful Style Sheets",
                                "answer": "a"
                            }
                            ,{
                                "enunciate": "¿Cómo se hace en CSS para que un enlace se muestre sin el subrayado?",
                                "a": "a) a {underline:no-underline}",
                                "b": "b) a {underline:none}",
                                "c": "c) a {text-decoration:no-underline}",
                                "d": "d) a {text-decoration:none}",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "¿Cómo se hace en CSS para que cada palabra en un texto comience con una letra en mayúsculas?",
                                "a": "a) text-transform:capitalize",
                                "b": "b) text-transform:uppercase",
                                "c": "c) text-transform:first-letter",
                                "d": "d) No se puede hacer con CSS",
                                "answer": "a" 
                            }
                            ,{
                                "enunciate": "En CSS, para definir el espacio entre el borde de un elemento y los elementos que lo rodean se emplea la propiedad",
                                "a": " a) border",
                                "b": " b) margin",
                                "c": " c) padding",
                                "d": " d) Las anteriores respuestas no son correctas",
                                "answer": "b" 
                            }
                            ,{
                                "enunciate": "¿Cómo se hace en CSS para que el símbolo de los elementos de una lista sea un cuadrado?",
                                "a": " a) type: square",
                                "b": " b) list-style-type: square",
                                "c": " c) list-type: square",
                                "d": " d) style-list: square",
                                "answer": "b" 
                            }
                            ,{
                                "enunciate": "En CSS, ¿qué propiedad se emplea para definir el orden de visualización cuando varios elementos se superponen?",
                                "a": " a) alpha-index",
                                "b": " b) tab-index",
                                "c": " c) vertical-index",
                                "d": " d) Las anteriores respuestas no son correctas",
                                "answer": "d" 
                            }
                            ,{
                                "enunciate": "¿Cómo se hace en CSS para que un enlace se muestre sin el subrayado?",
                                "a": " a) a {underline:no-underline}",
                                "b": " b) a {underline:none}",
                                "c": " c) a {text-decoration:no-underline}",
                                "d": " d) a {text-decoration:none}",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "En CSS, para cancelar los efectos de los elementos 'flotantes' se emplea la propiedad",
                                "a": " a) clear",
                                "b": " b) display",
                                "c": " c) unfloat",
                                "d": " d) Las anteriores respuestas no son correctas",
                                "answer": "a"
                            }
                            ,{
                                "enunciate": "En CSS, ¿qué valor de la propiedad overflow fuerza a que las barras de desplazamiento siempre estén visibles, aunque no sean necesarias?",
                                "a": " a) always",
                                "b": " b) auto",
                                "c": " c) visible",
                                "d": " d) Las anteriores respuestas no son correctas",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "En CSS, ¿qué propiedad se emplea para cambiar el cursor o puntero del ratón?",
                                "a": " a) cursor",
                                "b": " b) mouse",
                                "c": " c) pointer",
                                "d": " d) Las anteriores respuestas no son correctas",
                                "answer": "a"
                            }
                            ,{
                                "enunciate": "En CSS, ¿cómo se escribe un selector para que una regla se aplique a todos los párrafos que sean descendientes directos (hijos) de un elemento artículo (article)?",
                                "a": " a) article p",
                                "b": " b) article,p",
                                "c": " c) article>p",
                                "d": " d) article+p",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "¿Cuál es el lugar correcto en un documento HTML para hacer referencia a una hoja de estilo externa?",
                                "a": " a) Al principio del documento",
                                "b": " b) En la sección <head>",
                                "c": " c) En la sección <body>",
                                "d": " d) Al final del documento",
                                "answer": "b"
                            }
                            ,{
                                "enunciate": "¿Cuál es la sintaxis correcta de CSS para que todos los elementos <p> aparezcan en negrita?",
                                "a": " a) p {text-decoration:bold}",
                                "b": " b) p {text-size:bold}",
                                "c": " c) p {text-style:bold}",
                                "d": " d) p {font-weight:bold}",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "¿Qué propiedad de CSS se emplea para definir el espacio entre palabras?",
                                "a": " a) word-spacing",
                                "b": " b) letter-spacing",
                                "c": " c) white-space",
                                "d": " d) text-indent",
                                "answer": "a"
                            }
                            ,{
                                "enunciate": "¿Qué propiedad de CSS se emplea para indicar que un texto se debe mostrar en cursiva (itálica)?",
                                "a": " a) font-italic",
                                "b": " b) font-style",
                                "c": " c) font-variant",
                                "d": " d) Las anteriores respuestas no son correctas",
                                "answer": "b"
                            }
                            ,{
                                "enunciate": "¿Qué propiedad no existe en CSS?",
                                "a": " a) border-color",
                                "b": " b) border-line",
                                "c": " c) border-style",
                                "d": " d) border-width",
                                "answer": "b"
                            }
                            ,{
                                "enunciate": "¿Cuál es la propiedad de CSS que permite cambiar el tamaño del texto?",
                                "a": " a) text-size",
                                "b": " b) text-style",
                                "c": " c) font-size",
                                "d": " d) font-style",
                                "answer": "c"
                            }
                            ,{
                                "enunciate": "¿Qué etiqueta de HTML se emplea para definir una hoja de estilo externa?",
                                "a": " a) <link>",
                                "b": " b) <script>",
                                "c": " c) <style>",
                                "d": " d) Las anteriores respuestas no son correctas",
                                "answer": "a"
                            }
                            ,{
                                "enunciate": "¿Cómo se inserta un comentario en CSS?",
                                "a": " a) # esto es un comentario",
                                "b": " b) // esto es un comentario",
                                "c": " c) ' esto es un comentario",
                                "d": " d) /* esto es un comentario*/",
                                "answer": "d"
                            }            
                        ]
                    },
                    {
                        "Javascript":[
                            {
                                "enunciate": "¿Un fichero externo con código JavaScript debe contener la etiqueta <script>?",
                                "a": "a) Sí",
                                "b": "b) No",
                                "c": "b) Las respuestas anteriores son correctas",
                                "d": "b) No lo sé",
                                "answer": "b"
                            }
                            ,{
                                "enunciate": "En JavaScript, ¿cómo se abre una ventana nueva del navegador?",
                                "a": "a) document.open('pagina.html')",
                                "b": "b) document.new('pagina.html')",
                                "c": "c) window.open('pagina.html')",
                                "d": "d) window.new('pagina.html')",
                                "answer": "c"
                            }        
                            ,{
                                "enunciate": "En JavaScript, ¿cómo se inserta un comentario que ocupa una línea?",
                                "a": "a) <!-- Comentario -->",
                                "b": "b) // Comentario",
                                "c": "c) ' Comentario",
                                "d": "d) Las anteriores respuestas no son correctas",
                                "answer": "b"
                            }
                            ,{
                                "enunciate": "En JavaScript, ¿cuál es la forma correcta de escribir la cadena 'Hola mundo' en una página web?",
                                "a": "a) document.print('Hola mundo')",
                                "b": "b) document.write('Hola mundo')",
                                "c": "c) window.print('Hola mundo')",
                                "d": "d) window.write('Hola mundo')",
                                "answer": "b"
                            }        
                            ,{
                                "enunciate": "En JavaScript, el operador para concatenar cadenas es",
                                "a": "a) '&'",               
                                "b": "b) '+'",
                                "c": "c) '.'",
                                "d": "d) Las anteriores respuestas no son correctas",
                                "answer": "b"
                            }
                            ,{
                                "enunciate": "En JavaScript, ¿qué función se emplea para convertir una cadena a minúsculas?",
                                "a": "a) lower()",
                                "b": "b) lowerCase()",
                                "c": "c) toLower()",
                                "d": "d) toLowerCase()",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "En JavaScript, ¿cómo se calcula el máximo de los números 2 y 4?",
                                "a": "a) ceil(2, 4)",
                                "b": "b) top(2, 4)",
                                "c": "c) Math.ceil(2, 4)",
                                "d": "d) Math.max(2, 4)",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "En JavaScript, ¿cómo se muestra una ventana con el mensaje 'Hola mundo!'?",
                                "a": "a) alert('Hola mundo!');",
                                "b": "b) alertBox = 'Hola mundo!';",
                                "c": "c) alertBox('Hola mundo!);",
                                "d": "d) msgBox('Hola mundo!);",
                                "answer": "a"
                            }
                            ,{
                                "enunciate": "Para reemplazar todas las ocurrencias de la letra 'e' en una cadena de texto por un caracter en blanco se usa",
                                "a" : " 'aeiou aeiou aeiou' .findAndReplace('e', '')",
                                "b" : " 'aeiou aeiou aeiou' .replaceAll('e', '')",
                                "c" : " 'aeiou aeiou aeiou' .replace(/e/g, '')",
                                "d" : " 'aeiou aeiou aeiou' .replace('e', '')",
                                "answer": "c"
                            }
                            ,{
                                "enunciate": "En un documento HTML, para cargar un archivo javascript externo se codifica con las siguientes etiquetas",
                                "a" : " <import file='archivo.js'</script>",
                                "b" : " <link href='archivo.js' />",
                                "c" : " <script src='archivo.js'></script>",
                                "d" : " <script rel='archivo.js'></script>",
                                "answer": "c"
                            }
                            ,{
                                "enunciate": "¿Cuál de las siguientes expresiones es correcta?",
                                "a" : " a == b : true ? false",
                                "b" : " a == b ? (true : false)",
                                "c" : " a == b ! true ? false",
                                "d" : " a == b ? true : false",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "Para determinar la longitud de una cadena de texto se utiliza el método",
                                "a" : " 'abcdef'.count",
                                "b" : " count('abcdef')",
                                "c" : " 'abcdef'.length",
                                "d" : " length('abcdef')",
                                "answer": "c"
                            }
                            ,{
                                "enunciate": "La expresión !!undefined es igual a",
                                "a" : " true",
                                "b" : " false",
                                "c" : " void",
                                "d" : " undefined",
                                "answer": "b"
                            }
                            ,{
                                "enunciate": "¿Qué es una función anónima?",
                                "a" : " Las funciones anónimas no están soportadas en Javascript",
                                "b" : " Un método que no devuelve valor",
                                "c" : " Una función sin etiqueta que la defina",
                                "d" : " Una función que no reside en memoria",
                                "answer": "c"
                            }
                            ,{
                                "enunciate": "¿Cuál de las siguientes es una forma correcta de recorrer una colección de datos de tipo Array?",
                                "a" : " [4, 5, 6].forEach(function(i){ return i})",
                                "b" : " [4, 5, 6].each(function(i){ return i})",
                                "c" : " [4, 5, 6].collect(function(i){ return i})",
                                "d" : " [4, 5, 6].for(function(i){ return i})",
                                "answer": "b"
                            }
                            ,{
                                "enunciate": "La evaluación de ['a', 'b', 'c', 'd'].filter(function(letra){return letra == 'c' }) retorna",
                                "a" : "['a', 'b', 'd']",
                                "b" : "[]",
                                "c" : "c",
                                "d" : "['c']",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "Para definir una función de nombre miFunción escribimos",
                                "a" : "miFuncion function(){},",
                                "b" : "function: miFuncion(){};",
                                "c" : "function miFuncion(){}",
                                "d" : "function = miFuncion(){}",
                                "answer": "c"
                            }
                            ,{
                                "enunciate": "¿Cómo se hace para abrir una nueva ventana en el navegador?",
                                "a" : "window.open('http://www.google.es'),",
                                "b" : "window.new('http://www.google.es');",
                                "c" : "window.target('http://www.google.es')",
                                "d" : "window.popup('http://www.google.es')",
                                "answer": "a"
                            }
                            ,{
                                "enunciate": "Para mostrar un mensaje por pantalla se suele usar la siguiente instrucción",
                                "a" : "show('Esto es un mensaje'),",
                                "b" : "popup('Esto es un mensaje');",
                                "c" : "alert('Esto es un mensaje')",
                                "d" : "msgBox('Esto es un mensaje')",
                                "answer": "c"
                            }
                            ,{
                                "enunciate": "¿Qué operador se usa para conocer el tipo de una variable u objeto?",
                                "a" : "type,",
                                "b" : "isinstance;",
                                "c" : "typeof",
                                "d" : "is_a?",
                                "answer": "c"
                            }
                            ,{
                                "enunciate": "¿Cuál de las siguientes funciones devuelve el resultado ['a', 'b', 'c', 'd', 'e', 'f']?",
                                "a" : "['a', 'b', 'c'].concat(['d', 'e', 'f']),",
                                "b" : "['a', 'b', 'c'].split(['d', 'e', 'f']);",
                                "c" : "['a', 'b', 'c'].join(['d', 'e', 'f'])",
                                "d" : "['a', 'b', 'c'].merge(['d', 'e', 'f'])",
                                "answer": "a"
                            }
                            ,{
                                "enunciate": "Para convertir una cadena de texto 'a,b,c' a un objeto de tipo Array ['a', 'b', 'c'] usamos",
                                "a" : "'a,b,c'.join(',')",
                                "b" : "'a,b,c'.extract(',');",
                                "c" : "'a,b,c'.toArray()",
                                "d" : "'a,b,c'.split(',')",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "Para obtener si un valor es distinto de otro se utiliza la siguiente combinación de operadores",
                                "a" : "a != b",
                                "b" : "a not b",
                                "c" : "a - b = 0",
                                "d" : "a <> b",
                                "answer": "a"
                            }
                            ,{
                                "enunciate": "Para obtener el elemento <div id='menu'></div> se puede utilizar la función",
                                "a" : "document.getElementById('menu'),",
                                "b" : "document.getElementsByName('menu');",
                                "c" : "document.getId('menu')",
                                "d" : "document.getElementsByClassName('menu')",
                                "answer": "a"
                            }
                            ,{
                                "enunciate": "¿Cómo se escribe un comentario de una línea en Javascript?",
                                "a" : "-- Esto es un comentario,",
                                "b" : "# Esto es un comentario;",
                                "c" : "<!-- esto es un comentario -->",
                                "d" : "// Esto es un comentario",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "Para redondear hacia arriba un valor al siguiente entero la función correcta es",
                                "a" : "Math.fround(),",
                                "b" : "Math.round();",
                                "c" : "Math.ceil()",
                                "d" : "Math.floor()",
                                "answer": "c"
                            }
                            ,{
                                "enunciate": "Para encontrar la posición de un elemento dentro de un array, ¿cuál es la insrucción correcta?",
                                "a" : "['a', 'b', 'c', 'd'].get('c'),",
                                "b" : "['a', 'b', 'c', 'd'].positionOf('c');",
                                "c" : "['a', 'b', 'c', 'd'].indexOf('c')",
                                "d" : "['a', 'b', 'c', 'd'].getItem('c')",
                                "answer": "c"
                            }
                            ,{
                                "enunciate": "El valor de 1/0 es",
                                "a" : "Error,",
                                "b" : "0;",
                                "c" : "ZeroDivisionError",
                                "d" : "Infinity",
                                "answer": "d"
                            }
                            ,{
                                "enunciate": "En JavaScript, ¿cómo se escribe una sentencia condicional para comprobar que la variable 'i' es igual a 5? ",
                                "a": "a) if i=5 then",
                                "b": "b) if(i=5)",
                                "c": "c) if i==5 then",
                                "d": "d) if(i==5)",
                                "answer": "d"
                            }
                        ]
                    }
                ]
            },
            {
                "select":[
                    {
                        "HTML":[
                            {

                            }
                            ,{

                            }
                        ]
                    }
                ]
            },
            {
                "checkbox":[
                    {
                        "HTML":[
                            {

                            }
                            ,{

                            }
                        ]
                    }                
                ]
            }
        ]